/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.buscaminas;

/**
 *
 * @author Sergio tinoco
 */
public class Celda {
    public static final char CELDA_DESCUBIERTA = '2';
    public static final char CELDA_CUBIERTA = '1';
    public static final char BANDERA =  '3';
             
    private int minasContador;  
    private boolean tieneMina;  
    private char estadoCelda;    
    

     Celda() {
      this.tieneMina = false;
      this.minasContador = 0;
      this.estadoCelda = CELDA_CUBIERTA;
    }

    public boolean isMina() {
      return tieneMina;
    }
    
    public void setTieneMina(boolean tieneMina) {
      this.tieneMina = tieneMina;
    }

    int getMinasContador() {
      return minasContador;
    }

    void setMinasContador(int minasContador) {
      this.minasContador = minasContador;
      
    }

    public char getEstadoCelda() {
        return estadoCelda;
    }

    public void setEstadoCelda(char estadoCelda) {
        this.estadoCelda = estadoCelda;
    }
     
}

