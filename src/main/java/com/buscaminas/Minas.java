/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.buscaminas;

import java.util.Random;
import java.util.logging.Logger;
import java.util.logging.Level;

/**
 * En esta clase crearemos el tablero y colocaremos las minas
 *
 * @author Sergio Tinoco
 */
public class Minas {

    private static final Logger LOGGER = Logger.getLogger(Minas.class.getName());
    //FIXME: Crear la matriz cuando se sepa el tamaño que va a tener

    private final int FILAS;
    private final int COLUMNAS;
    private final int NUM_MINAS;
    private Celda[][] tableroBuscaminas;
    private final int HAY_MINA=9;

    public Minas(int filas, int columnas, int numeroMinas) {
        Random nuevaMina = new Random();

        if (numeroMinas >= filas * columnas) {
            LOGGER.warning("Las minas son iguales o superiores al numero de celdas en el tablero");
            numeroMinas = filas * columnas;
        }

        FILAS = filas;
        COLUMNAS = columnas;
        NUM_MINAS = numeroMinas;
        tableroBuscaminas = new Celda[FILAS][COLUMNAS];

        for (int f = 0; f < filas; f++) {

            for (int c = 0; c < columnas; c++) {
                tableroBuscaminas[f][c] = new Celda();

            }
        }
        int contadorDeMinas = 0;
        int filaNumAleatorio;
        int columnaNumAleatorio;

        //Esto nos dará un numero aleatorio en la fila y otro en la columna
        //Colocar minas en posiciones aleatorias, controlando que 
        //una mina no puede ocupar una celda donde ya se encontraba otra mina.
        do {

            filaNumAleatorio = nuevaMina.nextInt(FILAS);
            columnaNumAleatorio = nuevaMina.nextInt(COLUMNAS);
            if (!tableroBuscaminas[filaNumAleatorio][columnaNumAleatorio].isMina()) {
                tableroBuscaminas[filaNumAleatorio][columnaNumAleatorio].setTieneMina(true);
                LOGGER.log(Level.FINEST, "Se generó mina en la fila: " + filaNumAleatorio
                        + " y en la columna: " + columnaNumAleatorio);
                contadorDeMinas++;
            } else {
                LOGGER.log(Level.FINEST, "No se generó la mina en la fila: " + filaNumAleatorio
                        + " ni en la columna: " + columnaNumAleatorio);
            }

            LOGGER.log(Level.FINEST, "MUESTRA: " + contadorDeMinas + " Y TAMBIEN: " + numeroMinas);
        } while (contadorDeMinas < numeroMinas);
        

        //recuento de las minas 
        for (int f = 0; f < filas; f++) {
            for (int c = 0; c < columnas; c++) {
                if (tableroBuscaminas[f][c].isMina()) {
                    tableroBuscaminas[f][c].setMinasContador(HAY_MINA);
                }else{
                    
                    // En este bucle vamos a recorrer las celdas que rodean a la casilla
                    int minasContador = 0;
                    for (int s = f - 1; s <= f + 1; s++) {
                        for (int r = c - 1; r <= c + 1; r++) {
                             if (s >= 0 && s < filas && r >= 0 && r < columnas) {
                                if (tableroBuscaminas[s][r].isMina()) {
                                minasContador++;
                            
                                }
                            }
                        }
                    }
                   tableroBuscaminas[f][c].setMinasContador(minasContador);
                   LOGGER.finest("La celda que se encuentra en la posición: " + f + "," + c + " tiene alrededor " + minasContador + " minas");
                 //Guardamos el numero de minas que tenemos en
                // setMinasContador.   
                }
            }
        }
        LOGGER.log(Level.FINE, "Minas: \n" + this.toString());

    }
    
    public boolean descubrirCelda(int filaX, int columnaY) {
        tableroBuscaminas[filaX][columnaY].setEstadoCelda(Celda.CELDA_DESCUBIERTA);
        LOGGER.log(Level.FINEST, String.valueOf(filaX + " " + columnaY));
        if (tableroBuscaminas[filaX][columnaY].isMina()) {
            LOGGER.log(Level.FINE, this.toString());
            return true;
        } else if (tableroBuscaminas[filaX][columnaY].getMinasContador() > 0) {
            LOGGER.log(Level.FINE, this.toString());
            return false; 
        } else {
            for (int i = filaX - 1; i <= filaX + 1; i++) {
                for (int j = columnaY - 1; j <= columnaY + 1; j++) {
                    if (i > -1 && j > -1 && i < FILAS && j < COLUMNAS) {
                        if (tableroBuscaminas[i][j].getEstadoCelda() == Celda.CELDA_CUBIERTA) {
                            this.descubrirCelda(i, j);
                        }
                    }
                }
            }
        }
        LOGGER.log(Level.FINE, this.toString());
        return false;
    }
    /**
     * Método que nos permite poner las banderas en el juego
     *
     * @param filaX 
     * 
     * @param columnaY 
     * 
     * Estos dos parametros son las coordenadas del tablero donde queremos poner la bandera
     * 
     * @return Nos retorna si se ha podido colocar la mina o no
     *
     */
    
     public boolean banderaCelda(int filaX, int columnaY) {
         /*accede a la celda indicada y comprueba en primer lugar si la celda
         está descubierta, si lo está no hará nada, solo indicará 
         en el valor de retorno que no se puede poner la bandera.*/

        if (tableroBuscaminas[filaX][columnaY].getEstadoCelda() == Celda.CELDA_DESCUBIERTA) {
            LOGGER.log(Level.FINE, "No se ha podido colocar la bandera por que la celda está descubierta");
            LOGGER.log(Level.FINE, this.toString());
            return false;
        }
        /*si la celda ya está marcada con una bandera, se eliminará la marca
        de la bandera y el estado de la celda pasará a oculta.
        Además, este método retorna que no se ha podido colocar la bandera.*/

         if (tableroBuscaminas[filaX][columnaY].getEstadoCelda() == Celda.BANDERA) {
            LOGGER.log(Level.FINE, "No se ha podido colocar la bandera porque la celda está marcada ya como bandera  ");
            tableroBuscaminas[filaX][columnaY].setEstadoCelda(Celda.CELDA_CUBIERTA);
            LOGGER.log(Level.FINE, this.toString());
            return false;
        }
         /*la celda estaba sin descubrir. Aquí se coloca la 
         bandera cambiando el estado de la celda a marcada con bandera, y se
         retornara que sí se ha podido colocar la bandera.*/
       if (tableroBuscaminas[filaX][columnaY].getEstadoCelda() == Celda.CELDA_CUBIERTA) {
            LOGGER.log(Level.FINE, "La celda está oculta y se ha colocado bandera");
            tableroBuscaminas[filaX][columnaY].setEstadoCelda(Celda.BANDERA);
            LOGGER.log(Level.FINE, this.toString());
            return true;
        }
        return true;
    }
     /**
     * Método con el cual llevamos un contador de las banderas que se han puesto
     *
     * @return Devuelve el número de banderas colocadas
     */
    public int contadorBanderas() {
        int contadorBanderas = 0;
        for (int f = 0; f < FILAS; f++) {
            for (int c = 0; c < COLUMNAS; c++) {
                if (tableroBuscaminas[f][c].getEstadoCelda() == Celda.BANDERA) {
                    contadorBanderas++;
                }
            }
        }
        return contadorBanderas;
    }
    public void finPartida() {
        int celdasDescubiertas = 0;
        for (int f = 0; f < FILAS; f++) {
            for (int c = 0; c < COLUMNAS; c++) {
                if (tableroBuscaminas[f][c].getEstadoCelda() == Celda.CELDA_DESCUBIERTA) {
                    celdasDescubiertas++;
                }
            }
        }
        if(celdasDescubiertas + NUM_MINAS == FILAS * COLUMNAS){
            LOGGER.log(Level.FINEST, "Fin de la partida");
        }
    }
    
            

    
    @Override
    public String toString() {
        String mensaje = "";
        for (int f = 0; f < FILAS; f++) {

            for (int c = 0; c < COLUMNAS; c++) {
                if (tableroBuscaminas[f][c].isMina()) {
                    mensaje += "*";
                    LOGGER.log(Level.FINEST, "Las coordenadas de la fila son: " + f
                            + " las coordenadas de la columna son: " + c + " Hay mina");
                } else {
                    LOGGER.log(Level.FINEST, "Las coordenadas de la fila son: " + f
                            + " las coordenadas de la columna son: " + c + " No hay mina");
                    mensaje += ".";
                }
            }
            mensaje += "\n";
        }
        for (byte f = 0; f < FILAS; f++) {
            for (byte c = 0; c < COLUMNAS; c++) {
                    mensaje += tableroBuscaminas[f][c].getMinasContador();
                
            }
            mensaje += "\n";
        }
        mensaje += "Estado Actual: \n";
        for (int f = 0; f < FILAS; f++) {
            for (int c = 0; c < COLUMNAS; c++) {
                mensaje += tableroBuscaminas[f][c].getEstadoCelda();
            }
            mensaje += "\n";
        }
        mensaje+= "Contador de Banderas: ";
        mensaje+= contadorBanderas();
        return mensaje;
        
    }
}
