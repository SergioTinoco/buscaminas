package com.buscaminas;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;

public class Buscaminas extends Application {
   

    @Override
    public void start(Stage stage) {
        StackPane root = new StackPane();
        HBox ventana = new HBox();
        Button botonComprobar = new Button("Comprueba");
        Button botonBandera = new Button("Bandera");
        TextField fila = new TextField();
        TextField columna = new TextField();
        
        Scene scene = new Scene(root, 800, 600);
        root.getChildren().add(ventana);
        ventana.getChildren().add(fila);
        ventana.getChildren().add(columna);
        ventana.getChildren().add(botonComprobar);
         ventana.getChildren().add(botonBandera);
        ventana.setAlignment(Pos.CENTER);
        stage.setScene(scene);
        stage.show();
       
        
        stage.getIcons().add(new Image(Buscaminas.class.getResourceAsStream("/icon.png")));

        stage.setScene(scene);
        stage.show();
        Minas mina = new Minas(4,5,2);
        System.out.println(mina.toString());
        
        botonComprobar.setOnAction(new EventHandler<ActionEvent>() {            
            @Override
            public void handle(ActionEvent event) {
                int numFila = Integer.valueOf(fila.getText());
                int numColumna = Integer.valueOf(columna.getText());
                mina.descubrirCelda(numFila, numColumna);
            }
        });
        botonBandera.setOnAction(new EventHandler<ActionEvent>() {            
            @Override
            public void handle(ActionEvent event) {
                int posicionFila = Integer.valueOf(fila.getText());
                int posicionColumna = Integer.valueOf(columna.getText());
                mina.banderaCelda(posicionFila, posicionColumna);
                System.out.println(mina.toString());
            }
        });
            
        

    }

}
